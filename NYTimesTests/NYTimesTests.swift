//
//  NYTimesTests.swift
//  NYTimesTests
//
//  Created by shiekh on 1/29/21.
//

import XCTest
import RxSwift
@testable import NYTimes

class NYTimesTests: XCTestCase {
    let viewModel = MainViewModel()
    private var disposeBag = DisposeBag()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testGetArticlesApi(){
        viewModel.getArticles()
          let promise = expectation(description: "got successful Response")
        viewModel.statusCodeViewData.subscribe(onNext: {statusCode in
            if statusCode == 200 {
              promise.fulfill()
            } else {
              XCTFail("Status code: \(statusCode)")
            }
        }).disposed(by: disposeBag)
        wait(for: [promise], timeout: 3)
    }

}
