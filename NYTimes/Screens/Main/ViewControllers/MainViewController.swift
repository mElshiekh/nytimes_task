//
//  MainViewController.swift
//  NYTimes
//
//  Created by shiekh on 1/29/21.
//

import UIKit
import RxCocoa
import RxSwift

class MainViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = MainViewModel()
    private var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        bindViewData()
        viewModel.getArticles()
    }
    
    func setupTableView() {
        tableView.register(ArticleTableViewCell.self, forCellReuseIdentifier: ArticleTableViewCell.identifier)
    }
    
    private func bindViewData() {
        viewModel.articlesViewData.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: ArticleTableViewCell.identifier, cellType: ArticleTableViewCell.self)) { (row, element, cell) in
                cell.setup(title: element.title, subject: element.section, date: element.publishedDate)
            }.disposed(by: disposeBag)
        
        tableView.rx.modelSelected(Article.self).subscribe(onNext:  { [weak self] article in
            let vc = self?.instantiateViewController(viewControllerId: .DetailsViewController, StoryboardId: .Main) as! DetailsViewController
            vc.article = article
            self?.navigationController?.pushViewController(vc, animated: true)
        })
        .disposed(by: disposeBag)
    }

}
