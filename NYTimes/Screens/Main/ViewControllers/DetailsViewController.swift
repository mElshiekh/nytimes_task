//
//  DetailsViewController.swift
//  NYTimes
//
//  Created by shiekh on 1/29/21.
//

import UIKit

class DetailsViewController: UIViewController {
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var dateLable: UILabel!
    @IBOutlet weak var abstractLable: UILabel!
    @IBOutlet weak var subjectLable: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var article: Article?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
    }
    
    func setupData() {
        titleLable.text = article?.title
        dateLable.text = article?.publishedDate
        abstractLable.text = article?.abstract
        subjectLable.text = article?.section
        if let imageURL = article?.media?.first?.mediaMetadata?.last?.url {
            imageView.setImageWith(urlString: imageURL)
        }else {
            imageView.setDefaultImage()
        }
    }
}
