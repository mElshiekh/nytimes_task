//
//  MainViewModel.swift
//  NYTimes
//
//  Created by shiekh on 1/29/21.
//

import RxCocoa
import RxSwift
import Moya

class MainViewModel {

    private var disposeBag = DisposeBag()
    private var provider = MoyaProvider<ArticlesService>()
    var articlesViewData = BehaviorRelay<[Article]>(value: [])
    var isLoadingViewData = BehaviorRelay<Bool?>(value: nil)
    var errorViewData = BehaviorRelay<String?>(value: nil)
    var statusCodeViewData = PublishSubject<Int?>()
    
    func getArticles() {
        isLoadingViewData.accept(true)
        provider.rx.request(.PolpularArticles).subscribe { [weak self] event in
            self?.isLoadingViewData.accept(false)
            switch event {
            case let .success(response):
                self?.statusCodeViewData.onNext(response.statusCode)
                let parsedData: ArticlesResponse? = NetworkParser.shared.parseResponse(data: response.data)
                self?.articlesViewData.accept(parsedData?.results ?? [])
            case let .error(error):
                self?.errorViewData.accept(error.localizedDescription)
                print(error)
            }
        }.disposed(by: disposeBag)
    }
    
}
