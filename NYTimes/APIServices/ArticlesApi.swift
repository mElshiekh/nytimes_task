//
//  ArticlesApi.swift
//  NYTimes
//
//  Created by shiekh on 1/29/21.
//

import Foundation
import Moya


enum ArticlesService {
    case PolpularArticles
}

extension ArticlesService: TargetType, AccessTokenAuthorizable {
    
    
    var baseURL: URL { return URL(string: NetworkConstants.baseURL)! }
    
    var path: String {
        switch self {
        case .PolpularArticles:
            return NetworkConstants.popularArticles
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .PolpularArticles:
            return Method.get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .PolpularArticles:
            return .requestParameters(parameters: [NetworkConstants.ParametersNames.apiKey: NetworkConstants.apiKey], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        [:]
    }
    
    
    var authorizationType: AuthorizationType? {
        switch self {
        case .PolpularArticles:
            return .none
        }
    }
}
