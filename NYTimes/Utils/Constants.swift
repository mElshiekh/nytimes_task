//
//  Constants.swift
//  NYTimes
//
//  Created by shiekh on 1/29/21.
//

import Foundation

class NetworkConstants {
    static var apiKey = "ItMWnDeC2UwBokuvWxVzRHSQ8yoszuLa"
    static var baseURL = "https://api.nytimes.com/"
    static var popularArticles = "svc/mostpopular/v2/viewed/1.json"
    
    class ParametersNames {
        static var apiKey = "api-key"
    }
}



enum ViewControllerID: String {
    case MainViewController
    case DetailsViewController
}

enum StoryboardID: String {
    case Main
}


