//
//  UIViewControllerExtension.swift
//  NYTimes
//
//  Created by shiekh on 1/29/21.
//

import UIKit
extension UIViewController {
    func instantiateViewController(viewControllerId: ViewControllerID, StoryboardId: StoryboardID) -> UIViewController? {
        let storyboard = UIStoryboard(name: StoryboardId.rawValue, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId.rawValue)
        return controller
    }
    
    class func instantiateViewController(viewControllerId: ViewControllerID, StoryboardId: StoryboardID) -> UIViewController? {
        let storyboard = UIStoryboard(name: StoryboardId.rawValue, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId.rawValue)
        return controller
    }
}
