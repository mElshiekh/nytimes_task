//
//  NetworkParser.swift
//  NYTimes
//
//  Created by shiekh on 1/29/21.
//

import Foundation
import RxSwift
import RxCocoa


class NetworkParser {
    static let shared = NetworkParser()
    private init() {}
    
    func parseResponse<T>(data: Data?) -> T? where T: Codable {
        if let _ = data {
            do {
                let response = try JSONDecoder().decode(T.self, from: data!)
                return response
            } catch {
                print("Handle Error here")
            }
        }
        return nil
    }
}
