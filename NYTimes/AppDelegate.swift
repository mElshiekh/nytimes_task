//
//  AppDelegate.swift
//  NYTimes
//
//  Created by shiekh on 1/29/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        setRoot()
        return true
    }
    

    func setWindow() {
        window = UIWindow()
        window?.makeKeyAndVisible()
    }

    func setRoot() {
        setWindow()
        let mainVC = UIViewController.instantiateViewController(viewControllerId: .MainViewController, StoryboardId: .Main)!
        window?.rootViewController = UINavigationController(rootViewController: mainVC)
    }


}

