//
//  ArticleResponse.swift
//  NYTimes
//
//  Created by shiekh on 1/29/21.
//

import Foundation

// MARK: - ArticleResponse
class ArticlesResponse: Codable {
    var results: [Article]?

    init(results: [Article]?) {
        self.results = results
    }
}
